from flask import Flask, render_template
from flask_socketio import SocketIO, emit
import serial
import threading
import time
import datetime

app = Flask(__name__)
socketio = SocketIO(app)

ser = None
data = {"temperature": None, "humidity": None}
dataList = {"temperature": [], "humidity": []} 
connected = False
reading_thread = None
reading_thread_stop_event = threading.Event()
named_tuple = time.localtime()


def read_sensor_data():
    global data
    while not reading_thread_stop_event.is_set():
        if ser and ser.in_waiting > 0:
            read_ser = ser.readline().decode().strip()
            if read_ser:
                if read_ser.startswith("Temperature:"):
                    try:
                        temp = float(read_ser.split(":")[1].strip())
                        data["temperature"] = temp
                    except ValueError:
                        pass
                elif read_ser.startswith("Humidity:"):
                    try:
                        hum = float(read_ser.split(":")[1].strip())  
                        data["humidity"] = hum
                    except ValueError:
                        pass
                # Emit data to the client
                time_string = time.strftime("%Y-%m-%d %H:%M:%S")
                dataList["temperature"].insert(0,{"Timestamp": time_string, "Temperature": data["temperature"], "Humidity": data["humidity"]})

                socketio.emit('sensor_data', data);
            time.sleep(1)  
            

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('connect_device')
def handle_connect_device():
    global ser, connected
    if not connected:
        try:
            ser = serial.Serial("/dev/ttyUSB0", 9600, timeout=1)
            connected = True
            emit('status', {'status': 'Connected to dev/ttyUSB0'})
        except Exception as e:
            emit('status', {'status': f'Connection failed: {e}'})
    else:
        emit('status', {'status': 'Already connected'})
        
@socketio.on('disconnect_device')
def handle_disconnect_device():
    global ser, connected, reading_thread,data, dataList
    if connected:
        try:
            ser = ser.close();
            ser = None;
            connected = False;
            reading_thread = None
            data = {"temperature": None, "humidity": None}
            dataList = {"temperature": [], "humidity": []} 
            socketio.emit('sensor_data', data);
            emit('file_data', [])
            print('DISC', connected);
            emit('status', {'status': 'Disconnected from /dev/ttyUSB0'})
        except Exception as e:
            emit('status', {'status': f'Disconnect failed: {e}'})
    else:
        emit('status', {'status': 'Already disconnected'})


@socketio.on('start_reading')
def handle_start_reading():
    global reading_thread, reading_thread_stop_event
    if connected and (reading_thread is None or not reading_thread.is_alive()):
        print("Start button")
        reading_thread_stop_event.clear()
        reading_thread = threading.Thread(target=read_sensor_data)
        reading_thread.start()
        emit('status', {'status': 'Started reading data'})
    else:
        emit('status', {'status': 'Already started or not connected'})

@socketio.on('stop_reading')
def handle_stop_reading():
    global reading_thread_stop_event
    if connected:
        reading_thread_stop_event.set()
        emit('status', {'status': 'Stopped reading data'})
    else:
        emit('status', {'status': 'Not connected'})

@socketio.on('save_to_txt')
def handle_save_to_txt():
    global dataList
    if dataList:
        with open("weather.txt", "w") as f:
            for dataFile in dataList["temperature"]:
                f.write("%s\n" % dataFile)
            for dataFile in dataList["humidity"]:
                f.write("%s\n" % dataFile)
        #f.write(str(dataList["temperature"]))
        #emit('status-file', {'status': 'File saved'})
        f.close()
        
@socketio.on('load_from_file')
def handle_load_from_file():
    try:
        with open("weather.txt", "r") as f:
            file_data = [eval(line.strip()) for line in f.readlines()]
        emit('file_data', file_data)
        if file_data:
            emit('status-file', {'status': 'File loaded'})
    except Exception as e:
        emit('status-file', {'status-file': f'Failed to load file: {e}'})
    
@socketio.on('only_temp')
def handle_load_only_temp():
    emit('status', {'status': 'Reading only temperature'})
    
@socketio.on('only_hum')
def handle_load_only_hum():
    emit('status', {'status': 'Reading only humidity'})
    
if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=5000, debug=True)
